package View

import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Pos
import javafx.scene.layout.BorderPane
import tornadofx.*


class Views: View(){
   override val root = BorderPane()

    val counter = SimpleIntegerProperty()

    init {
        title = "Counter"

        with (root) {
            style {
                padding = box(all = 20.px)
            }

            center {
                vbox(10.0) {
                    alignment = Pos.CENTER

                    label() {
                        bind(counter)
                        style { fontSize = 25.px }
                    }

                    button("Click to increment").setOnAction {
                        increment()
                    }
                }
            }
        }
    }

    fun increment() {
        counter.value += 1
    }
}