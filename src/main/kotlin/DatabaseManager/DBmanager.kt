package DatabaseManager

import DatabaseManager.DB_ENV.*
import com.mongodb.ConnectionString
import com.mongodb.async.client.*
import java.util.logging.Logger


enum class DB_ENV {
    TEST,PRODUCTION
}

class DBmanager(var env: DB_ENV = PRODUCTION){

    private lateinit var databaseName :String

    companion object {
        private val log = Logger.getLogger(DBmanager::class.java.name)
    }

    init{
        if(env == DB_ENV.PRODUCTION){
            databaseName = "NPI_Production_DB"
        }else{
            databaseName = "NPI_TESTING_DB"
        }
    }

    private fun connect(serverURL: String = "mongodb://localhost:27017"): MongoClient{
        return MongoClients.create(ConnectionString(serverURL))
    }

    fun getDatabase() {
        connect().getDatabase(WorkorderMaster.databaseName)
    }


}