package Routing

/*
    WORouting has all the routing which is required for the the WO
    extra_routing is a collection of station recorded in the status url (*some weird stuff which need to attention from the TPM)
 */
data class WORouting (var routing: MutableList<String>,
                      val PDGN:String="PDGN",
                      val REWORK_SSMT:String = "REWORK-SSMT",
                      val REWORK_SMASSY:String= "REWORK-SMASSY",
                      val DVAL:String="DVAL",val SVAL:String="SVAL",
                      val OQC_REJECT:String = "OQC_REJECT",
                      var extra_routing:MutableList<String>)
