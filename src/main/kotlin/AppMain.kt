import DatabaseManager.DB_ENV
import DatabaseManager.DBmanager
import View.Views
import WebScrapper.Mode
import WebScrapper.TCardScrapper
import WebScrapper.getShipOutDate
import com.mongodb.async.client.MongoClients
import javafx.application.Application


fun main(args: Array<String>) {

    Application.launch(WorkOrderTracker::class.java,*args)

    println("Please Insert Work Order into the Work Order Tracker.")
    var workOrder:String?
    do {
        workOrder = readLine()
        if(workOrder == ""){
            println("Please insert work order.")
        }
    } while(workOrder == "")



    TCardScrapper(workOrder!!,Mode.Information)


}