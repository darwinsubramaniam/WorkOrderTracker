package WebScrapper


import Routing.WORouting
import org.jsoup.Jsoup
import java.util.*
import WebScrapper.Mode as Mode
import java.util.logging.Logger

/*
    This class take the information from the
    WOInformation Class to populate the logic
    The WorkOrder Data class
 */
data class WorkOrder(var information:WOInformation, var routing: WORouting)

/*
    WOInformation has all the information which is projected in the TCard status. This is important to understand the purpose of the build
    only thing which required by the TPM to fill up is the purpose which is not optional.
 */
data class WOInformation(val workOrder:String,var OrderQty:Int,
                         var status:WorkOrderStatus?,var WipQty:Int,
                         var pendingRACQty:Int,var description:String,var productfamily:String?,var rejecQty:Int,
                         var specialInstruction:String? = "" , var purpose:String?, var sku:String,var releaase_date:Calendar)




class TCardScrapper(private var workOrder: String, mode:Mode = Mode.Status){


    companion object {
        val LOG = Logger.getLogger(TCardScrapper::class.java.name)
    }

    init {
        LOG.info("Work Order ${this.workOrder} is inserted into the scrapper")


        when(mode){
            Mode.Status -> getWorkFlowInfo()
            Mode.Routing -> getRouting()
            Mode.Information -> getInformation()
        }

    }

    /*
        isNewWo will ensure the setting up the part of the information is different compared to the one which already exist.
        1. Check if the work is in the database .
        2.
     */
    fun isNewWO():Boolean{
        var isNew:Boolean = true

        //Logic for testing the WO with one in the database.

        return isNew
    }

    fun getPurpose():String?{
        var purpose:String? = null

        return purpose
    }

    fun getWorkFlowInfo(){
        //url for reading the status for this work order.
        val url = "http://mvele.sandisk.com/orders/$workOrder"
        LOG.info("Reading Status of Work Order $workOrder.")
        var mainPage = Jsoup.connect(url).get()
        var workFlowInfoContainer = mainPage.getElementById("workflow_info")



    }

    fun getRouting(){
        //url for reading the routing for this work order.
        val tcardRoutingURL = "http://mvele.sandisk.com/tcard/"
        LOG.info("Reading Routing for Work Order $workOrder.")
    }

    fun getInformation(): WOInformation{
        val url: String = "http://mvele.sandisk.com/orders/$workOrder"
        LOG.info("Getting Information for Work Order $workOrder")
        var mainPage = Jsoup.connect(url).get()
        var descriptionContainer = mainPage.getElementById("description")
        return WOInformation(workOrder = workOrder,
                OrderQty = getOrderQty(descriptionContainer),
                status = getOrderStatus(descriptionContainer),
                WipQty = getWIPQty(descriptionContainer),
                pendingRACQty = getPendingRACQty(descriptionContainer),
                description = getDescription(descriptionContainer),
                productfamily = getProductFamily(descriptionContainer),
                purpose = getPurpose(descriptionContainer),
                specialInstruction = getSpecialInstruction(descriptionContainer),
                sku = getSKU(descriptionContainer),
                rejecQty = getRejectQty(descriptionContainer),
                releaase_date = getReleaseDate(descriptionContainer))
    }





}