package WebScrapper

import org.jsoup.nodes.Element
import java.time.Instant
import java.util.*
import java.util.logging.Logger

val Log = Logger.getLogger("WorkOrderInformation.kt")

fun getOrderQty(informationContainer:Element):Int{
    var orderQty:Int = 0
    //Text which is been used it identify the text contain the information after this .
    var identifier =  "Order Qty:"
    for(informationChildren in informationContainer.children()){
        for (orderContainer in informationChildren.children())
            if(orderContainer.text().contains(identifier)){
                orderQty =  orderContainer.text().removePrefix(identifier).trim().toInt()
                Log.info("Order Quantity is $orderQty")
                if(orderQty == 0){
                    error("orderQty is zero")
                }
          }
    }
    return orderQty
}

fun getSKU(informationContainer:Element):String{
    var sku:String = ""
    //Text which is been used it identify the text contain the information after this .
    var identifier =  "Part Number:"
    for(informationChildren in informationContainer.children()){
        for (skuContainer in informationChildren.children())
            if(skuContainer.text().contains(identifier)){
                sku =  skuContainer.text().removePrefix(identifier).trim()
                Log.info("SKU : $sku")
                if(sku  ==  ""){
                    error("SKU is empty")
                }
            }
    }
    return sku
}

fun getOrderStatus(informationContainer:Element):WorkOrderStatus?{
    var status:WorkOrderStatus? = null
    /** inner Function **/
    fun Release(){
        status = WorkOrderStatus.Release
    }
    fun Closed(){
        status = WorkOrderStatus.Closed
    }
    /**Text which is been used it identify the text contain the information after this .**/
    var identifier =  "Order Status:"
    /**main logic for this function. **/
    for(informationChildren in informationContainer.children()) {
        for (statusContainer in informationChildren.children())
            if (statusContainer.text().contains(identifier)) {
                var statusText = statusContainer.text().removePrefix(identifier).trim()
                when (statusText){
                    "Released" -> Release()
                    "Closed" -> Closed()
                    else -> error("getOrderStatus in WorkOrderInformation is giving Problem. Logic might be change.")
                }
                Log.info("Order Status is ${status.toString()}")
            }
    }
    return status
}

fun getWIPQty(informationContainer:Element):Int{
    var  wipQty:Int = 0

    /**Text which is been used it identify the text contain the information after this .**/
    var identifier =  "WIP Qty:"
    /**main logic for this function. **/
    for(informationChildren in informationContainer.children()) {
        for (wipContainer in informationChildren.children())
            if (wipContainer.text().contains(identifier)) {
                wipQty = wipContainer.text().removePrefix(identifier).trim().toInt()
                Log.info("Wip Quantity : $wipQty")
            }
    }
    return wipQty
}

fun getDescription(informationContainer:Element):String {
    var description:String = ""
    //Text which is been used it identify the text contain the information after this .
    var identifier =  "Description:"

    for(informationChildren in informationContainer.children()){
        for (descriptionContainer in informationChildren.children())
            if(descriptionContainer.text().contains(identifier)){
                description =  descriptionContainer.siblingElements().text()
                Log.info("Description : $description")
                if(description  ==  ""){
                    error("Description is empty")
                }
            }
    }
    return description
}

fun getRejectQty(informationContainer:Element):Int{
    var rejectQty:Int = 0
    /**Text which is been used it identify the text contain the information after this .**/
    var identifier =  "Reject TR Qty:"
    /**main logic for this function. **/
    for(informationChildren in informationContainer.children()) {
        for (RejectContainer in informationChildren.children())
            if (RejectContainer.text().contains(identifier)) {
                rejectQty = RejectContainer.text().removePrefix(identifier).trim().toInt()
                Log.info("Reject Quantity : $rejectQty")
            }
    }
    return rejectQty
}

fun getPendingRACQty(informationContainer:Element):Int{
    var pendingRACQty:Int = 0
    /**Text which is been used it identify the text contain the information after this .**/
    var identifier =  "Pending RAC Qty:"
    /**main logic for this function. **/
    for(informationChildren in informationContainer.children()) {
        for (pendingContainer in informationChildren.children())
            if (pendingContainer.text().contains(identifier)) {
                pendingRACQty = pendingContainer.text().removePrefix(identifier).trim().toInt()
                Log.info("Pending Quantity : $pendingRACQty")
            }
    }
    return  pendingRACQty
}

fun getSpecialInstruction(informationContainer: Element):String?{
    var specialInstruction:String? = null
    //Text which is been used it identify the text contain the information after this .
    var identifier =  "Special Instructions:"

    var parent = informationContainer.parent().getElementsByTag("pre")
    specialInstruction = parent.text()
    Log.info("Special Instruction : $specialInstruction")
    return specialInstruction
}

fun getShipOutDate(workorder:String): Calendar{
    Log.info(Date.from(Instant.now()).toString())
    return Calendar.getInstance()
}

fun getReleaseDate(informationContainer: Element):Calendar{
    var releaseDate= GregorianCalendar.getInstance()
    //Text which is been used it identify the text contain the information after this .
    var identifier =  "Release Date:"
    for(informationChildren in informationContainer.children()){
        for (releaseDateContainer in informationChildren.children())
            if(releaseDateContainer.text().contains(identifier)){
                var tcard_date_format =  releaseDateContainer.text().removePrefix(identifier).trim().split("-")
                releaseDate = GregorianCalendar(tcard_date_format[0].toInt(),tcard_date_format[1].toInt(),tcard_date_format[2].substringBefore(" ").toInt())
                Log.info("Release Date : $releaseDate")
                if(tcard_date_format.iterator().hasNext().toString() ==  ""){
                    error("Description is empty")
                }
            }
    }
    return releaseDate
}

fun setShipoutDate(shipoutDate:Date){

}

fun getPurpose(informationContainer: Element):String?{
    var purpose:String? = null
    Log.info("Running getPurpose")
    return purpose
}

fun getProductFamily(informationContainer:Element):String?{

    var productFamily:String? = null
    Log.info("Running getProductFamily")
    return productFamily
}