import WebScrapper.Mode
import WebScrapper.TCardScrapper
import WebScrapper.WOInformation
import WebScrapper.WorkOrderStatus
import org.bouncycastle.math.raw.Mod
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.util.*


class WorkOrderInformationTest{

    private  val tCardScrapper : WOInformation =TCardScrapper("828123",Mode.Information).getInformation()


    @Test
    fun releaseDate(){

        var calender = GregorianCalendar(2018,4,16)
        Assertions.assertEquals(calender, tCardScrapper.releaase_date)
    }

    @Test
    fun orderQty(){

        Assertions.assertEquals(36,tCardScrapper.OrderQty)
    }

    @Test
    fun sku(){

        Assertions.assertEquals("SDAPMUW-128G-1101",tCardScrapper.sku)
    }

    @Test
    fun orderStatus(){

        Assertions.assertEquals(WorkOrderStatus.Release,tCardScrapper.status)
    }

    @Test
    fun wipQty(){

        Assertions.assertEquals(3,tCardScrapper.WipQty)
    }

    @Test
    fun description(){

        Assertions.assertEquals("Western Digital PC SN520 128G,PCIe NVMe TritonMp28 4CH, BiCS3 256Gb X3 2P,M.2 2242,non-SED-Lenovo LBG",tCardScrapper.description)
    }

    @Test
    fun rejectQty(){

        Assertions.assertEquals(1,tCardScrapper.rejecQty)
    }

    @Test
    fun pendingRACQty(){
        Assertions.assertEquals(0,tCardScrapper.pendingRACQty)
    }

    @Test
    fun specialInstruction(){
        Assertions.assertEquals("Deviation : QS1",tCardScrapper.specialInstruction)
    }
}